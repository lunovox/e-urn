![screenshot]

# Menus da URNA ELETRÔNICA

## Menu "Inicial"

* Botão "Eleição Presidencial [toMenu: "Eleição Presidencial"]
* Botão "Tribunal Eleitoral" [NeedPriv:"ElectoralJudge"; toMenu: "Tribunal Eleitoral"]
* Botão "Sair" [tecla "ESC"]

## Menu "Eleição Presidencial"

* Botão "Eleja Agora" [VisibleIf: "Acessado Por Urna + Maioridade de 72 horas de gameplay"; InVisibleIf: "InternetDevice"; toMenu: "Escolha seu Presidente"]
* Botão "Presidente Atual" [toMenu: "Dados do Presidente Eleito"]
* Botão "Cadidatos Presidenciais" [toMenu: "Cadidatos Presidenciais"]
* Botão "Voltar" [toMenu: "Inicial"; VisibleIf: Acessado Por Urna; InVisibleIf: InternetDevice;]

### Menu "Cadidatos Presidenciais"

* Lista "Candidatos Presidenciais" (Selecionável)
* Textarea "Propaganda do Candidato Selecionado"
* Botão "Branco" [toMenu: "Painel de Aviso", toMensagem: "Você anulou seu voto!"]
* Botão "Confirmar" [toMenu: "Voto Confirmado"]
* Botão "Cancelar" [toMenu: "Eleição Presidencial"]

#### Menu "Painel de Aviso"

* Imagem "Logotipo da Urna Eketrônics"
* Etiqueta Titulo "URNA ELETRÔNICA"
* Etiqueta "<Mensagem>"
* Botão "Voltar" [toMenu: "Eleição Presidencial"]

#### Menu "Voto Confirmado"

* Imagem "Rosto do Candidato"
* Etiqueta Titulo "<Nome do Cadidato>"
* Etiqueta Mensagem: "Seu voto foi contabilizado. Obrigado!"
* Botão "Voltar" [toMenu: "Eleição Presidencial"]

## Menu "Tribunal Eleitoral"

* Botão "Regras da Eleição" [toMenu: "Regras da Eleição Presidencial"]
* Botão "Candidatar-se a President" [VisibleIf: "Acessed per UrnDevice + Maioridade de 72 horas de gameplay"; InVisibleIf:"Acessed per InternetDevice"; toMenu: "Ficha do Candidato"]
* Botão "Cancelar Candidatura" [VisibleIf: "Acessed per UrnDevice + Maioridade de 72 horas de gameplay"; InVisibleIf:"Acessed per InternetDevice"; toMenu: "Ficha do Candidato"]
* Botão "Configurações da Eleições" [toMenu:"Eleição Presidencial"; VisibleIf: "Acessed per UrnDevice and WithPriv:ElectoralJudge" ; InVisibleIf: "Acessed per InternetDevice or WithOutPriv:ElectoralJudge";]
* Botão "Voltar" [toMenu: "Inicial"; VisibleIf: "Acessed per UrnDevice"; InVisibleIf:"Acessed per InternetDevice"; ]

## Menu "Configurações da Eleições"

Etiqueta "Maioridade Eleitoral"

* Campo: Maioridade dos Eleitores [Padrão: 72 horas reais de gameplay] : Seleciona quantas horas o jogador terá que ter jogado para poder ser um eleitor.
* Campo: Maioridade dos Candidatos [Padrão: 168 horas reais de gameplay]  : Seleciona quantas horas o jogador terá que ter jogado para poder ser um candidato a presidente.
* Campo "Período Probatório" [Padrão: 1 mês real de empoderamento]  : Seleciona o tempo máximo que o Presidente terá o poder em suas mãos. Antes de provar que ainda possui a maioria do apoio polular.
* Campo "Vencimento do Voto" [Padrão: 6 meses reais de Prescrição de Voto] : Seleciona o tempo máximo que o voto de um jogador ficará em posse do presidente escolhido individualmente pelo eleito. O eleitor pode deve renovar seu voto antes de acabar o seu tempo individual de prescrição de voto.
* Botão "Salvar e Voltar"  [toMenu: "Confirm Settings"]
* Botão "Voltar sem salvar" [toMenu: "Tribunal Eleitoral"]

--------------------------------------------------------

# Comando de quem possuir o privilègio "ElectoralJudge"

* ````/president <playername>```` : Seleciona um jogador como presidente forçadamente. 
* ````/candidate <playername>```` : Seleciona um jogador como candidato forçadamente.
* ````/nullvote <playername>```` : Anula forçadamente o voto de um específico jogador. Mas, não apagará o registro do tempo de gameplay do jopgador eleitor.
* ````/nullcandidate <playername>```` : Anula forçadamente a candidatura de um específico jogador.  Mas, não apagará o registro do tempo de gameplay do jopgador candidato. Se o jogador candidato anulado for o Presidente. O servidor selecionará o segundo lugar dos candidatos como novo presidente. Caso não exista um segundo lugar dos candidatos, o servidor ficará sem presidente. 

--------------------------------------------------------

# Tarefas Futuras Neste Mod

Estou pensando em criar um mod de "urna eletrônica". Para fazer os jogadores votarem em quem será o presidente do servidor. 

O presidente eleito terá o poder de:

* Escolher o valor dos custos da proteção de terreno que serão pagos por cada jogador.
* Utilizar o dinheiro do jogo recolhido daa proteção de terreno para: Indenizar jogadores por alienações de terreno, financiar quests para funcionários públicos [tipo mandar fazer: pontes,estrada, praças, e etc], dar prêmios em dinheiro de eventos semanais [torneio de pvp, concurso de pixelart], e financiar pesquisa e desenvolvimento de mods.
* Tombar edificações para se tornarem patrimônios históricos. (é tipo uma isenção da tarifa de proteção de terreno para prédios bonitos).
* Nomear Delegados (moderadores que irão banir jogadores ruins).
* Nomear Médicos (Jpgadores que podem ressucitar outro que bugaram e não conseguem respawar).

Os jogadores também poderão poderão a qualquer momento pegar o seu voto de volta, e votar em outro candidato. [é uma espécie de impeachment]

--------------------------------------------------------



[screenshot]:https://gitlab.com/lunovox/e-urn/-/raw/master/textures/text_eurn_front.png?inline=false
