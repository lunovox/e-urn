--[[
elected = { 
		president = {
			name = "", --name of the elected candidate.
			when = 0, --when the candidate was elected.
		},
	},
candidates = { 
		president = {
			["cadidatename"] = {
				political_campaign = "",
				voters = { --names of voters who voted for this candidate.
					["votername"] = "when",
				}, 
			},
		},
	},
--]]

--############################################################################

modEUrn.getPropCommPresident = function()
	return {
      --privs = {electoraljudge=true},
      params = "[<".. modEUrn.translate("candidate_name")..">]",
      description = modEUrn.translate("Show or Select the presidente of the server."),
      func = function(playername, candidatename)
      	if type(candidatename)=="string" and candidatename~="" then
      		if minetest.get_player_privs(playername).electoraljudge then
		      	if modEUrn.doPresident(playername, candidatename) then
			      	modEUrn.doSave()
				      minetest.chat_send_player(
				      	playername, 
				      	--os.date("%Y-%m-%d %Hh:%Mm:%Ss", now) ..
				      	core.colorize("#00FF00", "[E-URN]").." "
				      	..modEUrn.translate("Player %s has been named the president of this server!"):format(dump(candidatename))
				      )
			      end
				else
		        	minetest.chat_send_player(
		         	playername, 
		         	--os.date("%Y-%m-%d %Hh:%Mm:%Ss", now) ..
		         	core.colorize("#FF0000", "[E-URN]").." "
		         	..modEUrn.translate("You do not have the 'electoraljudge' privilege to define who will be the president of the server!")
		         )
				end
			else
				minetest.chat_send_player(
	         	playername, 
	         	--os.date("%Y-%m-%d %Hh:%Mm:%Ss", now) ..
	         	core.colorize("#00FF00", "[E-URN]").." "
	         	..modEUrn.translate("The president of this server is %s!"):format(dump(modEUrn.getPresidentName()))
	         )
			end
      end,
   }
end

minetest.register_chatcommand(
	"president", 
	modEUrn.getPropCommPresident()
)
--[[ 
minetest.register_chatcommand(
	modEUrn.translate("president"), 
	modEUrn.getPropCommPresident()
)
--]]

--############################################################################

modEUrn.getPropCommCandidateMe = function()
	return {
      --privs = {electoraljudge=true},
      params = "[<".. modEUrn.translate("political campaign")..">]",
      description = modEUrn.translate("Register your campaign to run for server president."),
      func = function(playername, political_campaign)
      	local player = minetest.get_player_by_name(playername)
      	if player and player:is_player()  then
      		--modEUrn.debug("playername='"..playername.."' | political_campaign='"..political_campaign.."'")
      		modEUrn.FormSpecs.showFormRegCampaign(playername, political_campaign)
      	else
		   	local candidatename = playername
		   	if modEUrn.doPresidentCandidate(playername, candidatename, political_campaign) then
		   		modEUrn.doSave()
				   minetest.chat_send_player(
				   	playername, 
				   	--os.date("%Y-%m-%d %Hh:%Mm:%Ss", now) ..
				   	core.colorize("#00FF00", "[E-URN]").." "
				   	..modEUrn.translate("Player @1 has been registered to run as a candidate for president of this server!", dump(playername))
				   )
		   	end
		   end
      end,
   }
end

minetest.register_chatcommand(
	"candidateme", 
	modEUrn.getPropCommCandidateMe()
)

--############################################################################

modEUrn.getPropCommDiscandidateMe = function()
	return {
      --privs = {electoraljudge=true},
      --params = "[<".. modEUrn.translate("political campaign")..">]",
      description = modEUrn.translate("Unregister your campaign to run for server president."),
      func = function(playername, param)
      	modEUrn.doCheckDataBase()
         
         if modEUrn.handler.candidates.president[playername]~=nil then
            modEUrn.handler.candidates.president[playername] = nil
         end
         modEUrn.handler.elected.president.name = ""
         modEUrn.handler.elected.president.when = 0
      	
      	modEUrn.doSave()
   	   minetest.chat_send_player(
   	   	playername, 
   	   	--os.date("%Y-%m-%d %Hh:%Mm:%Ss", now) ..
   	   	core.colorize("#00FF00", "[E-URN]").." "
   	   	..modEUrn.translate("Player @1 has been unregistered to run as a candidate for president of this server!", dump(playername))
   	   )
      end,
   }
end

minetest.register_chatcommand(
	"discandidateme", 
	modEUrn.getPropCommDiscandidateMe()
)
--############################################################################

modEUrn.getPropCommCandidates = function()
	return {
      --privs = {electoraljudge=true},
      --params = "[<".. modEUrn.translate("political campaign")..">]",
      description = modEUrn.translate("Show the name of all candidates for president."),
      func = function(playername)
      	local cands = modEUrn.getPresidentCandidates()
      	local candList = ""
			local candCount = 0
			--minetest.chat_send_all("Numero de Candidatos: "..#modEUrn.handler.candidates.president)
			for _, iCandPresName in ipairs(cands) do
				candCount = candCount + 1
		   	if candList == "" then
		   		candList = iCandPresName
		   	else
		   		candList = candList..", "..iCandPresName
		   	end
			end
			minetest.chat_send_player(
	      	playername, 
	      	core.colorize("#00FF00", "[E-URN]").." "
	      	..modEUrn.translate("Number of Candidates: %02d"):format(candCount)
	      )
	      minetest.chat_send_player(
	      	playername, 
	      	core.colorize("#00FF00", "[E-URN]").." "
	      	..modEUrn.translate("Candidate List: %s"):format(candList)
	      )
      end,
   }
end

minetest.register_chatcommand(
	"candidates", 
	modEUrn.getPropCommCandidates()
)

--############################################################################

modEUrn.getPropCommCandCampaign = function()
	return {
      --privs = {electoraljudge=true},
      params = "<".. modEUrn.translate("candidate_name")..">",
      description = modEUrn.translate("Show the Campaign of candidate for president."),
      func = function(playername, candidatename)
	      modEUrn.showPresCandCampaign(playername, candidatename)
      end,
   }
end

minetest.register_chatcommand(
	"campaign", 
	modEUrn.getPropCommCandCampaign()
)
minetest.register_chatcommand(
	"candidate", 
	modEUrn.getPropCommCandCampaign()
)

--############################################################################

modEUrn.getPropCommVoter = function()
	return {
      --privs = {electoraljudge=true},
      --params = "<".. modEUrn.translate("candidate_name")..">",
      description = modEUrn.translate("Check if you're ready to vote for a presidential candidate!"),
      func = function(playername, params)
         local voterTimePlayed = modEUrn.getVoterPlayedTime(playername)
         if type(voterTimePlayed)=="number" then
            local hours = math.ceil(voterTimePlayed/(60*60))
            local color, messege = ""
            if voterTimePlayed >= modEUrn.MinPlayedHours * (60*60) then
               messege = modEUrn.translate("Are you ready to vote!")
               color = "#00FF00"
            else
               messege = modEUrn.translate(
                  "You only have @1 hours of the @2 minimum game hours to be eligible to vote!",
                  string.format("%02d", tonumber(hours)),
                  string.format("%02d", tonumber(modEUrn.MinPlayedHours))
               )
               color = "#FF0000"
            end
            minetest.chat_send_player(
               playername, 
               core.colorize(color, "[E-URN]").." "..messege
            )
         end
      end,
   }
end

minetest.register_chatcommand(
	"voter", 
	modEUrn.getPropCommVoter()
)

--############################################################################

modEUrn.getPropCommCandVote = function()
	return {
      --privs = {electoraljudge=true},
      params = "<".. modEUrn.translate("candidate_name")..">",
      description = modEUrn.translate("Vote for a specific candidate for president."),
      func = function(playername, candidatename)
	      local resulte, cause = modEUrn.doVote(playername, candidatename)
	      local color = "#FF0000"
	      if resulte then
	         color = "#00FF00"
	         minetest.sound_play("sfx_eurn_confirm", {to_player=playername, max_hear_distance=5.0,})
	      else
	      	minetest.sound_play("sfx_failure", {to_player=playername, max_hear_distance=5.0,})
	      end
         minetest.chat_send_player(
				playername, 
				core.colorize(color, "[E-URN]").." "..cause
			)
      end,
   }
end

minetest.register_chatcommand(
	"vote", 
	modEUrn.getPropCommCandVote()
)

--############################################################################

modEUrn.getPropCommUnvote = function()
	return {
      --privs = {electoraljudge=true},
      --params = "<".. modEUrn.translate("candidate_name")..">",
      description = modEUrn.translate("Apply Blank Vote."),
      func = function(playername, param)
	      local resulte, cause = modEUrn.doUnvote(playername)
	      local color = "#FF0000"
	      if resulte then
	         color = "#00FF00"
	         minetest.sound_play("sfx_eurn_confirm", {to_player=playername, max_hear_distance=5.0,})
	      else
	      	minetest.sound_play("sfx_failure", {to_player=playername, max_hear_distance=5.0,})
	      end
         minetest.chat_send_player(
				playername, 
				core.colorize(color, "[E-URN]").." "..cause
			)
      end,
   }
end

minetest.register_chatcommand(
	"unvote", 
	modEUrn.getPropCommUnvote()
)
minetest.register_chatcommand(
	"whitevote", 
	modEUrn.getPropCommUnvote()
)

--############################################################################

modEUrn.getPropCommVotes = function()
	return {
      privs = {electoraljudge=true},
      --params = "<".. modEUrn.translate("candidate_name")..">",
      description = modEUrn.translate("Displays the number of votes for each candidate! (Need the 'electoraljudge' privilege!)"),
      func = function(playername, candidatename)
      	--modEUrn.debug("modEUrn.handler.candidates.president = "..dump(modEUrn.handler.candidates.president))
      	--modEUrn.debug("modEUrn.sortTableLenght(modEUrn.handler.candidates.president) = "..modEUrn.sortTableLenght(modEUrn.handler.candidates.president))
      	if type(modEUrn.handler.candidates.president)=="table" then
		   	local votes = {}
		   	for iCandPresName, iCandPresTable in pairs(modEUrn.handler.candidates.president) do
		   		local num = modEUrn.sortTableLenght(iCandPresTable.voters)
					if type(iCandPresTable.voters)=="table" and num >= 1 then
						votes[iCandPresName] = num
						--modEUrn.debug("num = "..num)
					end
				end
				
				if modEUrn.sortTableLenght(votes) >= 1 then
					--modEUrn.debug(dump(votes))
					votes = modEUrn.unsortTableByValue(votes)
					--modEUrn.debug(dump(votes))
					local strVotes = ""
					for iCandPresName, iCandPresVotes in pairs(votes) do
						--modEUrn.debug("modEUrn.getPropCommVotes(): iCandPresName = "..dump(iCandPresName).." | iCandPresVotes = "..dump(iCandPresVotes))
						if strVotes=="" then
							strVotes = modEUrn.translate("   * %s : %02d votes"):format(iCandPresName, iCandPresVotes)
						else
							strVotes = strVotes.."\n"..modEUrn.translate("   * %s : %02d votes"):format(iCandPresName, iCandPresVotes)
						end
					end
					minetest.chat_send_player(
						playername, 
						core.colorize("#00FF00", "[E-URN]").." "
						..modEUrn.translate("Voting list for presidential election: ").."\n"..strVotes
					)

					--]]

				else
					minetest.chat_send_player(
						playername, 
						core.colorize("#FF0000", "[E-URN]").." "
						..modEUrn.translate("No one has voted in the presidential election yet!")
						--..core.colorize("#00FF00", candidatename)
					)
				end
			else
				minetest.chat_send_player(
					playername, 
					core.colorize("#FF0000", "[E-URN]").." "
					..modEUrn.translate("There is no registered candidate for the presidential election!")
					--..core.colorize("#00FF00", candidatename)
				)
			end
		end,
   }
end

minetest.register_chatcommand(
	"votes", 
	modEUrn.getPropCommVotes()
)

--############################################################################

modEUrn.getPropCommElection = function()
	return {
      privs = {electoraljudge=true},
      --params = "<".. modEUrn.translate("candidate_name")..">",
      description = modEUrn.translate("Apply presidential selection vote counting! (Need the 'electoraljudge' privilege.)"),
      func = function(playername, param)
      	--modEUrn.debug("modEUrn.handler.candidates.president = "..dump(modEUrn.handler.candidates.president))
      	--modEUrn.debug("modEUrn.sortTableLenght(modEUrn.handler.candidates.president) = "..modEUrn.sortTableLenght(modEUrn.handler.candidates.president))
      	local result, cause = modEUrn.doCheckPresident()
	   	if result == true then
	   	   minetest.chat_send_all(
					core.colorize("#00FF00", "[E-URN]").." "..cause
				)
	   	else
	   	   minetest.chat_send_player(
					playername, 
					core.colorize("#FF0000", "[E-URN]").." "..cause
				)
	   	end
		end,
   }
end

minetest.register_chatcommand(
	"election", 
	modEUrn.getPropCommElection()
)
--############################################################################

--[[  
minetest.register_chatcommand(
	"t", 
	 {
      privs = {server=true},
      --params = "[<".. modEUrn.translate("political campaign")..">]",
      description = modEUrn.translate("Register your campaign to run for server president."),
      func = function(playername, param)
      	local t = { 
      		1, 
      		2, 
      		["três"] = {
      			a = 1,
      			b = 2,
      			c = 3,
      		},
      		4 
      	}
      	table.remove(t, 2)
      	t["três"].b = nil
      	minetest.chat_send_all(dump2(t))
      end,
   }
)
--]]
