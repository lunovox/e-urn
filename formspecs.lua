
		
modEUrn.FormSpecs = {
	showFormMain = function(playername)
      local myFormSpec = ""
		myFormSpec = myFormSpec
		.."formspec_version[6]"
		.."size[16,8,false]"
		.."background[0,-8;16,16;text_eurn_front.png]"
		.."box[2.05,1.50;7.75,5.00;#001100CC]"
		.."button[2.15,1.65;7.55,1;btnPresElection;"..modEUrn.translate("PRESIDENTIAL ELECTION").."]"
		.."button[2.15,2.65;7.55,1;btnElectoralCourt;"..modEUrn.translate("ELECTORAL COURT").."]"
		.."style_type[button_exit;bgimg=;bgimg_pressed=;border=;bgcolor=red]"
		.."button_exit[2.15,3.65;7.55,1;;"..modEUrn.translate("EXIT").."]"
		minetest.sound_play("sfx_eurn_button", {to_player=playername, max_hear_distance=5.0,})
		minetest.show_formspec(playername, "frmEUrnMain", myFormSpec)
	end,
   showFormPresElection = function(playername)
      local myFormSpec = ""
		myFormSpec = myFormSpec
		.."formspec_version[6]"
		.."size[16,8,false]"
		.."background[0,-8;16,16;text_eurn_front.png]"
		.."box[2.05,1.50;7.75,5.00;#001100CC]"
		.."button[2.15,1.65;7.55,1;btnPresCandidates;"..modEUrn.translate("PRESIDENTIAL CANDIDATES").."]"
		.."button[2.15,2.65;7.55,1;btnPresElect;"..modEUrn.translate("PRESIDENT ELECT").."]"
		.."style[btnBack;bgimg=;bgimg_pressed=;border=;bgcolor=red]"
		.."button[2.15,3.65;7.55,1;btnBack;"..modEUrn.translate("BACK").."]"
		minetest.sound_play("sfx_eurn_button", {to_player=playername, max_hear_distance=5.0,})
		minetest.show_formspec(playername, "frmEUrnPresElection", myFormSpec)
   end,
   showFormPresCands = function(playername, candidatename)
   	local voterTimePlayed = modEUrn.getVoterPlayedTime(playername)
   	local cands = modEUrn.getPresidentCandidates()
   	local candList = ""
		local candCount = 0
		local selected = 0
		for _, iCandPresName in ipairs(cands) do
			candCount = candCount + 1
	   	if candList == "" then
	   		candList = minetest.formspec_escape(iCandPresName)
	   	else
	   		candList = candList..","..minetest.formspec_escape(iCandPresName)
	   	end
	   	if type(candidatename)=="string" and candidatename==iCandPresName then
	   		selected = candCount
	   	end
		end
   	local myFormSpec = ""
		myFormSpec = myFormSpec
		.."formspec_version[6]"
		.."size[16,8,false]"
		.."background[0,-8;16,16;text_eurn_front.png]"
		--.."box[-0.5,-0.5;16.5,8.5;#001100CC]"
		.."box[2.05,1.50;7.75,5.00;#001100CC]"
		.."textlist[2.15,1.50;7.55,4;lstCands;"..candList..";"..selected..";true]"
		if type(voterTimePlayed)=="number" and voterTimePlayed >= modEUrn.MinPlayedHours * (60*60) then
         myFormSpec = myFormSpec
         .."style[btnWhiteVote;bgcolor=white;color=black]"
         .."button[2.10,5.50;2.25,1;btnWhiteVote;"..minetest.formspec_escape(modEUrn.translate("WHITE")).."]"
		end
		myFormSpec = myFormSpec
		.."style[btnBack;bgimg=;bgimg_pressed=;border=;bgcolor=red]"
		.."button[4.40,5.50;2.50,1;btnBack;"..minetest.formspec_escape(modEUrn.translate("CANCEL")).."]"
		if selected >= 1 and type(voterTimePlayed)=="number" and voterTimePlayed >= modEUrn.MinPlayedHours * (60*60) then
         myFormSpec = myFormSpec
         .."style[btnPresCandSel;bgimg=;bgimg_pressed=;border=;bgcolor=green]"
         .."button[6.95,5.50;2.75,1;btnPresCandSel;"..minetest.formspec_escape(modEUrn.translate("SELECT")).."]"
		end
		minetest.sound_play("sfx_eurn_button", {to_player=playername, max_hear_distance=5.0,})
		minetest.show_formspec(playername, "frmEUrnPresCands", myFormSpec)
   end,
   showFormPresCands_deprecated = function(playername, candidatename)
   	local cands = modEUrn.getPresidentCandidates()
   	local candList = ""
		local candCount = 0
		local selected = 0
		--minetest.chat_send_all("Numero de Candidatos: "..#modEUrn.handler.candidates.president)
		for _, iCandPresName in ipairs(cands) do
			candCount = candCount + 1
	   	if candList == "" then
	   		candList = minetest.formspec_escape(iCandPresName)
	   	else
	   		candList = candList..","..minetest.formspec_escape(iCandPresName)
	   	end
	   	if type(candidatename)=="string" and candidatename==iCandPresName then
	   		selected = candCount
	   	end
		end
   	
   	
   	local myFormSpec = ""
		myFormSpec = myFormSpec
		.."formspec_version[6]"
		.."size[16,8,false]"
		.."background[0,-8;16,16;text_eurn_front.png]"
		.."box[-0.5,-0.5;16.5,8.5;#001100CC]"
		.."style[lstCands;bgcolor=red]"
		.."style_type[textlist;bgcolor=red]"
		.."textlist[0.5,0.5;3,7;lstCands;"..candList..";"..selected..";true]"
      
		if selected >= 1 then
			local Campaign = modEUrn.getPresCandCampaign(candidatename)
			local body = [[
<center>
	<img name=text_candidate_face.png width=128 height=128>
	<bigger><b>%s</b></bigger>
</center>
<justify>%s</justify>
			]]
			myFormSpec = myFormSpec
			--.."style[htmPanel;bgcolor=white;border=#008800CC;color=black]"
			.."hypertext[4.0,0.5;11.25,6.00;htmPanel;"
			--..minetest.formspec_escape("<global margin=10 valign=0 color=#FF00FF hovercolor=#00FFFF size=12 font=normal halign=center >")
			.."<global valign=middle halign=center margin=30 background=#001100CC bordercolor=#008800CC color=#FFFFFF hovercolor=#00FF00 size=12 font=normal>"
			.."<tag name=action color=#FF0000 hovercolor=#00FF00 font=normal size=12>"
			.."<tag name=bigger color=#00CC00 font=normal size=28>"
			.."<tag name=big color=#CCCC00 font=normal size=18>"
			.."<tag name=b color=#00CC00 font=normal>"
			..minetest.formspec_escape(body:format(cands[selected], Campaign))
			.."]" -- Fim de hypertext[]
			
			local voterTimePlayed = modEUrn.getVoterPlayedTime(playername)
   		if type(voterTimePlayed)=="number" and voterTimePlayed >= modEUrn.MinPlayedHours * (60*60) then
            myFormSpec = myFormSpec
            .."style[btnWhiteVote;bgcolor=white;color=black]"
            .."button[4.0,6.75;3.00,1;btnWhiteVote;"..minetest.formspec_escape(modEUrn.translate("WHITE")).."]"
            .."style[btnPresVote;bgimg=;bgimg_pressed=;border=;bgcolor=green]"
            .."button[7.10,6.75;5.65,1;btnPresVote;"..minetest.formspec_escape(modEUrn.translate("CONFIRM")).."]"
			end
		end
		
		myFormSpec = myFormSpec
		.."style[btnBack;bgimg=;bgimg_pressed=;border=;bgcolor=red]"
		.."button[12.85,6.75;2.55,1;btnBack;"..minetest.formspec_escape(modEUrn.translate("CANCEL")).."]"
		minetest.sound_play("sfx_eurn_button", {to_player=playername, max_hear_distance=5.0,})
		minetest.show_formspec(playername, "frmEUrnPresCands", myFormSpec)
   end,
   showFormAlert = function(playername, icon, title, titlecolor, message, buttonname, buttonsound)
      local body = ""
      body = [[
<center>
   <img name=%s width=128 height=128>
   <style color=%s><bigger><b>%s</b></bigger></style>
   %s
</center>
		]]
		if type(titlecolor)=="nil" or titlecolor=="" then
         titlecolor = "#00FF00"
		end
		body = body:format(icon, titlecolor, title, message)
      local myFormSpec = ""
		myFormSpec = myFormSpec
		.."formspec_version[6]"
		.."size[16,8,false]"
		.."background[0,-8;16,16;text_eurn_front.png]"
		.."box[2.05,1.50;7.75,5.00;#001100CC]"
		.."hypertext[2.15,1.50;7.55,4.00;htmPanel;"
		--..minetest.formspec_escape("<global margin=10 valign=0 color=#FF00FF hovercolor=#00FFFF size=12 font=normal halign=center >")
		.."<global valign=middle halign=center margin=10 background=#001100CC color=#FFFFFF hovercolor=#00FF00 size=12 font=normal>"
		.."<tag name=action color=#FF0000 hovercolor=#00FF00 font=normal size=12>"
		.."<tag name=bigger font=normal size=28>"
		.."<tag name=big color=#CCCC00 font=normal size=18>"
		.."<tag name=b color=#00CC00 font=normal>"
		..minetest.formspec_escape(body)
		.."]" -- Fim de hypertext[]
		--.."style[btnBack;bgimg=;bgimg_pressed=;border=;bgcolor=red]"
		
		local button_type = "button"
      if type(buttonname)=="nil" or buttonname=="" or buttonname=="btnExit" then
         button_type = "button_exit"
      end
		myFormSpec = myFormSpec
		..button_type.."[2.15,5.50;7.55,1;"..minetest.formspec_escape(buttonname)..";OK]"
		if type(buttonsound)=="string" and buttonsound~="" then
		   minetest.sound_play(buttonsound, {to_player=playername, max_hear_distance=5.0,})
		end
		minetest.show_formspec(playername, "frmEUrnAlert", myFormSpec)
   end,
	showFormRegCampaign = function(playername, political_campaign)
		local Campaign = modEUrn.getPresCandCampaign(playername)
		if type(political_campaign)~="string" or political_campaign==""then
			political_campaign = Campaign
		end
		
		
		local myFormSpec = ""
		myFormSpec = myFormSpec
		.."formspec_version[6]"
		.."size[16,8,false]"
		.."background[0,-8;16,16;text_eurn_front.png]"
		.."box[0.5,0.75;15,5.75;#000000CC]"
		.."textarea[0.5,0.75;15,5.75;txtCampaign;"
			..minetest.formspec_escape(
				modEUrn.translate("Political Campaign of")..": "..
				core.colorize("#00FF00", playername)
			)..";"
			..minetest.formspec_escape(political_campaign)..
		"]"
		.."button_exit[0.5,6.75;12.25,1;btnSave;"..modEUrn.translate("REGISTER").."]"
		.."button_exit[12.85,6.75;2.55,1;;"..modEUrn.translate("CANCEL").."]"
		minetest.sound_play("sfx_eurn_button", {to_player=playername, max_hear_distance=5.0,})
		minetest.show_formspec(playername, "frmEUrnRegCampaign", myFormSpec)
	end,
	showFormPresVote = function(playername, candidatename)
		local myFormSpec = ""
		myFormSpec = myFormSpec
		.."formspec_version[6]"
		.."size[16,8,false]"
		.."background[0,-8;16,16;text_eurn_front.png]"
		.."box[-0.5,-0.5;16.5,8.5;#001100CC]"
		.."style[lstCands;bgcolor=red]"
		--.."style_type[textlist;bgcolor=red]"
		--.."textlist[0.5,0.5;3,7;lstCands;"..candList..";"..selected..";true]"
      
      local Campaign = modEUrn.getPresCandCampaign(candidatename)
		
		if type(modEUrn.handler.candidates.president[candidatename])=="nil" then 
		   Campaign = "" 
      end

		local body = [[
<center>
	<img name=text_candidate_face.png width=128 height=128>
	<bigger><b>%s</b></bigger>
</center>
<justify>%s</justify>
		]]
		myFormSpec = myFormSpec
		--.."style[htmPanel;bgcolor=white;border=#008800CC;color=black]"
		.."hypertext[4.0,0.5;11.25,6.00;htmPanel;"
		--..minetest.formspec_escape("<global margin=10 valign=0 color=#FF00FF hovercolor=#00FFFF size=12 font=normal halign=center >")
		.."<global valign=middle halign=center margin=30 background=#001100CC bordercolor=#008800CC color=#FFFFFF hovercolor=#00FF00 size=12 font=normal>"
		.."<tag name=action color=#FF0000 hovercolor=#00FF00 font=normal size=12>"
		.."<tag name=bigger color=#00CC00 font=normal size=28>"
		.."<tag name=big color=#CCCC00 font=normal size=18>"
		.."<tag name=b color=#00CC00 font=normal>"
		..minetest.formspec_escape(body:format(candidatename, Campaign))
		.."]" -- Fim de hypertext[]
		
		local voterTimePlayed = modEUrn.getVoterPlayedTime(playername)
		if type(voterTimePlayed)=="number" and voterTimePlayed >= modEUrn.MinPlayedHours * (60*60) then
         myFormSpec = myFormSpec
         .."style[btnWhiteVote;bgcolor=white;color=black]"
         .."button[4.0,6.75;3.00,1;btnWhiteVote;"..minetest.formspec_escape(modEUrn.translate("WHITE")).."]"
         .."style[btnPresVote;bgimg=;bgimg_pressed=;border=;bgcolor=green]"
         .."button[7.10,6.75;5.65,1;btnPresVote;"..minetest.formspec_escape(modEUrn.translate("CONFIRM")).."]"
		end
		myFormSpec = myFormSpec
		.."style[btnBack;bgimg=;bgimg_pressed=;border=;bgcolor=red]"
		.."button[12.85,6.75;2.55,1;btnBack;"..minetest.formspec_escape(modEUrn.translate("CANCEL")).."]"
		minetest.sound_play("sfx_eurn_button", {to_player=playername, max_hear_distance=5.0,})
		minetest.show_formspec(playername, "frmEUrnPresVote", myFormSpec)
		
		
		--[[  
		local Campaign = modEUrn.getPresCandCampaign(candidatename)
		--if type(modEUrn.handler.candidates.president[candidatename])=="nil" then Campaign = "" end
		local myFormSpec = ""
		myFormSpec = myFormSpec
		.."formspec_version[6]"
		.."size[16,8,false]"
		.."background[0,-8;16,16;text_eurn_front.png]"
		.."box[0.5,0.75;15,5.75;#00000088]"
		.."textarea[0.5,0.75;15,5.75;;"
			..minetest.formspec_escape(
				modEUrn.translate("Political Campaign of")..": "..
				core.colorize("#00FF00", candidatename)
			)..";"
			..minetest.formspec_escape(Campaign)..
		"]"
		.."button_exit[0.5,6.75;12.25,1;btnVote;"..modEUrn.translate("VOTE").."]"
		.."button_exit[13.25,6.75;2,1;;"..modEUrn.translate("CLOSE").."]"
		
		minetest.log('action', modEUrn.translate("Player %s is listing campaign data..."):format(playername))
		minetest.log('action',modEUrn.translate("Candidate Name")..candidatename)
		minetest.log('action',modEUrn.translate("Candidate Campaign")..Campaign)
		minetest.sound_play("sfx_eurn_button", {to_player=playername, max_hear_distance=5.0,})
		minetest.show_formspec(playername, "frmEUrnInfoCampaign", myFormSpec)
		--]]
	end,
	
	--[[
	showHypertext = function(playername)
		--Fonte: 
		-- * https://minetest.gitlab.io/minetest/formspec/
		-- * https://minetest.gitlab.io/minetest/formspec/#hypertextxywhnametext
		-- * https://github.com/minetest/minetest/blob/master/doc/lua_api.md
		
		local myFormSpec = ""
		myFormSpec = myFormSpec
		--.."formspec_version[5.6.0]"
		.."formspec_version[6]"
		.."size[16,8,false]"
		.."background[0,-8;16,16;text_eurn_front.png]"
		--.."bgcolor[000000;false;FFFFFF]"
		--.."bgcolor[#00880044;false;#000000]"
		--.."bgcolor[#636D7600;true]"
		--.."position[0,0]"
		--.."container[0,0]"
		--.."image[0,-8;16,16;text_eurn_front.png]"
		.."vertlabel[15.75,0.5;"..minetest.formspec_escape(modEUrn.translate("E-URN")).."]"
		.."hypertext[0.5,0.5;15,7;myPage;"
		--..minetest.formspec_escape("<global margin=10 valign=0 color=#FF00FF hovercolor=#00FFFF size=12 font=normal halign=center >")
		.."<global valign=middle halign=left margin=10 background=#FFFFFFCC color=#000000 hovercolor=#00FF00 size=12 font=normal>"
		.."<tag name=action color=#FF0000 hovercolor=#00FF00 font=normal size=12>"
		..minetest.formspec_escape(body:format(playername:upper()))
		.."]" -- Fim de hypertext[]
		--.."container_end[]"
		
		
		
		
		--"hypertext[<X>,<Y>;<W>,<H>;<name>;<text>]"
		minetest.show_formspec(playername, "frmEUrnInit", myFormSpec)
	end,
	--]]
}

minetest.register_on_player_receive_fields(function(player, formname, fields)
	local playername = player:get_player_name()
	--modEUrn.debug("formname: "..formname.."\n".."fields: "..dump(fields), playername)
	
	if type(formname)=="string" and formname == "frmEUrnAlert" then
      if type(fields.btnEUrnMain) ~= "nil" then
         modEUrn.FormSpecs.showFormMain(playername)
      elseif type(fields.btnEUrnPresElection) ~= "nil" then
         modEUrn.FormSpecs.showFormPresElection(playername)
      elseif type(fields.btnEUrnPresCands) ~= "nil" then
         modEUrn.FormSpecs.showFormPresCands(playername)
      end
	elseif type(formname)=="string" and formname == "frmEUrnMain" then
      if type(fields.btnPresElection) ~= "nil" then
         modEUrn.FormSpecs.showFormPresElection(playername)
      elseif type(fields.quit) ~= "true" then
		   minetest.sound_play("sfx_eurn_button", {to_player=playername, max_hear_distance=5.0,})
		end
   elseif type(formname)=="string" and formname == "frmEUrnPresElection" then
      if type(fields.btnPresCandidates) ~= "nil" then
      	modEUrn.FormSpecs.showFormPresCands(playername, nil)
      elseif type(fields.btnPresElect) ~= "nil" then
      	local icon, title, message, buttonname = ""
      	local presidentname = modEUrn.getPresidentName()
         if presidentname~="" then
            icon = "text_candidate_face.png"
            title = presidentname:upper()
            titlecolor = "#00FF00"
            message = modEUrn.translate("PRESIDENT ELECT")
            buttonname = "btnEUrnMain"
         else
            icon = "favicon.png"
            title = modEUrn.translate("E-URN")
            titlecolor = "#FF0000"
			   message = modEUrn.translate("No presidential candidate has yet been elected!")
			   buttonname = "btnEUrnPresElection"
         end
      	modEUrn.FormSpecs.showFormAlert(
      	   playername, 
            icon, 
            title, 
            titlecolor, 
            message, 
            buttonname,
            "sfx_eurn_button"
         )
      elseif type(fields.btnBack) ~= "nil" then
         modEUrn.FormSpecs.showFormMain(playername)
      end
   elseif type(formname)=="string" and formname == "frmEUrnPresCands" then
   	if type(modEUrn.selCand)~="table" then
			modEUrn.selCand = {}
		end
   	if type(fields.lstCands) ~= "nil" then
   		local lstCands = minetest.explode_textlist_event(fields.lstCands)
   		if type(lstCands.index)=="number" and lstCands.index>=1 then
   			local cands = modEUrn.getPresidentCandidates()
   			modEUrn.selCand[playername] = cands[lstCands.index]
   			modEUrn.FormSpecs.showFormPresCands(playername, cands[lstCands.index])
   		end
   	elseif type(fields.btnPresCandSel) ~= "nil" then
   	   if type(modEUrn.selCand[playername])=="string" and modEUrn.selCand[playername]~="" then
   	      modEUrn.FormSpecs.showFormPresVote(playername, modEUrn.selCand[playername])
   	   end
   	elseif type(fields.btnWhiteVote) ~= "nil" then
   	   modEUrn.showWhiteVote(playername)
   	elseif type(fields.btnBack) ~= "nil" then
  			modEUrn.FormSpecs.showFormPresElection(playername)
   	end
   elseif type(formname)=="string" and formname == "frmEUrnPresVote" then
   	if type(fields.btnPresVote) ~= "nil" then
   		if type(modEUrn.selCand[playername])=="string" and modEUrn.selCand[playername]~="" then
				local resulte, cause = modEUrn.doVote(playername, modEUrn.selCand[playername])
				local color = "#FF0000"
				local icon = "favicon.png"
            local title = modEUrn.translate("E-URN")
            local message = cause
            local buttonname = "btnEUrnPresCands"
            local buttonsound = "sfx_failure"
			   
				if resulte then
				   color = "#00FF00"
				   icon = "text_candidate_face.png"
               title = modEUrn.selCand[playername]:upper()
               message = modEUrn.translate("Thanks for your vote!")
               buttonname = "btnEUrnMain"
               buttonsound = "sfx_eurn_confirm"
				   --minetest.sound_play("sfx_eurn_confirm", {to_player=playername, max_hear_distance=5.0,})
				end
			   minetest.chat_send_player(
					playername, 
					core.colorize(color, "[E-URN]").." "..cause
				)
				modEUrn.FormSpecs.showFormAlert(
         	   playername, 
               icon, 
               title, 
               color,
               message, 
               buttonname,
               buttonsound
            )
   		end
   	elseif type(fields.btnWhiteVote) ~= "nil" then
   	   modEUrn.showWhiteVote(playername)
   	elseif type(fields.btnBack) ~= "nil" then
   	   local candidatename = ""
   	   if type(modEUrn.selCand)=="table" 
   	      and type(modEUrn.selCand[playername])=="string" 
   	      and modEUrn.selCand[playername]~="" 
         then
   	      candidatename = modEUrn.selCand[playername]
         end
   	   modEUrn.FormSpecs.showFormPresCands(playername, candidatename)
   	end
   elseif type(formname)=="string" and formname == "frmEUrnPresElect" then
   	if type(fields.btnBack) ~= "nil" then
  			modEUrn.FormSpecs.showFormPresElection(playername)
   	end
	elseif type(formname)=="string" and formname == "frmEUrnRegCampaign" then
		if type(fields.txtCampaign)~= "nil" and type(fields.btnSave) ~= "nil" then
			--modEUrn.debug("modEUrn.doPresidentCandidate(playername='"..playername.."', playername='"..playername.."', fields.txtCampaign="..dump(fields.txtCampaign)..")")
			if modEUrn.doPresidentCandidate(playername, playername, fields.txtCampaign) then
				modEUrn.doSave()
				minetest.chat_send_player(
					playername, 
					--os.date("%Y-%m-%d %Hh:%Mm:%Ss", now) ..
					core.colorize("#00FF00", "[E-URN]").." "
					..modEUrn.translate("Player @1 has been registered to run as a candidate for president of this server!", dump(playername))
				)
				minetest.sound_play("sfx_eurn_confirm", {to_player=playername, max_hear_distance=5.0,})
			end
		elseif type(fields.quit) ~= "true" then
		   minetest.sound_play("sfx_eurn_button", {to_player=playername, max_hear_distance=5.0,})
		end
	end
end)

