![urn_front]
# [E-URN]

[![minetest_icon]][minetest_link] [![baixa_icon]][baixa_link] [![projeto_icon]][projeto_link] <!-- [![bower_icon]][bower_link] -->  

Adds a presidential election system and an Electronic Urn as an item that helps collect votes.

All players can make promises as candidates.  However, only players with more than 72 hours of gameplay will be able to vote for a candidate.  The vote is permanent (except if the player has not played for more than 6 months).  You can vote at any time, whenever you want, as many times as you want and for as many people as you want.  Because the only valid vote is just the last one.  Every beginning of the real month there will be an automatic recount of votes.

Don't worry about a possible need for Impeachment for some unwary new president who commits some unpopular act.  If the players don't support the possibly unwary president.  It is enough for the majority of players to vote for another candidate.  At the end of the month, the candidate with the highest number of votes will be automatically elected.

## **License:**

* [![license_icon]][license_link]

 More details: 
 
[![wiki_en_icon]][wiki_en_link] [![wiki_pt_icon]][wiki_pt_link]

## **Developers:**

* Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [xmpp](xmpp:lunovox@disroot.org?join), [social web](http://qoto.org/@lunovox), [audio conference](https://meet.jit.si/MinetestBrasil), [more contacts](https:libreplanet.org/wiki/User:Lunovox)

## 📦 **Depends:**

| Mod Name | Dependency Type | Descryption |
| :--: | :--: | :-- |
| default | Mandatory |  Minetest Game Included. | 


## **Commands:**

These commands are not mandatory for you to use this mod.  To do the same, you can use the graphical mode of the electronic voting machine and the voter registration card.

| Command | Parameter | Descryption |
| :-- | :--: | :-- |
| ````/president```` | ````[<candidate_name>]```` | Show or Select the presidente of the server. Need the ````electoraljudge```` privilege. |
| ````/candidateme```` | ````[<political campaign>]```` | Register your campaign to run for server president. |
| ````/discandidateme```` | | Unregister your campaign to run for server president. |
| ````/candidates```` | | Show the name of all candidates for president. |
| ````/candidate```` or ````/campaign```` | ````[<candidate_name>]```` | Show the Campaign of candidate for president. |
| ````/vote```` | ````[<candidate_name>]```` | Vote for a specific candidate for president. Need a minimum number of hours of online play. |
| ````/unvote```` or ````/whitevote```` | | Apply Blank Vote. |
| ````/votes```` | | Show the name of all candidates for president with the amount of votes. Need the ````electoraljudge```` privilege. | 
| ````/election```` | | Apply presidential selection vote counting! Need the ````electoraljudge```` privilege. | 


## **API:**

Extras are functions of this mod that you can use in another mod you create.  For example: "create a mod that gives a special power or privilege that only the elected president can enjoy"

| Function | Descryption |
| :--: | :-- |
| ````<array> modEUrn.getPresidentCandidates()```` | Return a array with name of all presidente candidates of the server. Return empty array if not exist.
| ````<string/nil> modEUrn.getPresidentName()```` | Return a string with the presidente name of the server. Return nil if not exist.
| ````modEUrn.doSave()```` | Save database em file ````e-urm.db````.


## **CONFIGURATION:**

You don't need to worry about the settings below to make this mod work.  But, if you want to configure, you can change the settings through the graphical menu, or you can change them directly by editing the 'minetest.conf' file.

| Function | Descryption |
| :--: | :-- |
| ````eurn.debug = <true/false>```` | Allows you to print the debug information of this mod on the screen. Default: false |
| ````eurn.save_compressed = <true/false>```` | Whether the database will be BASE64 compressed. If enabled will save database bank without compression in file '.db'. Default: true |
| ````eurn.voter.min_played_hours = <number>```` | Minimum game time in hours to become a voter. Default: 90 | Min: 0 | Max: 8760 |
| ````eurn.crafting_recipe = <true/false>```` | Activate crafting recipe for all players got their own Electronic Urn. Default: false |

## **INTERNACIONALIZATION:**

### **Available Languages:**

* English (Defaul, Concluded: 100%)
* Português (Concluded: 0%)

### **Translate this mod to your Language:**

See more details in file: [locale/README.md]

[urn_front]:textures/text_eurn_front.png
[E-URN]:https://gitlab.com/lunovox/e-urn/
[baixa_icon]:https://img.shields.io/static/v1?label=Download&message=Mod&color=blue
[baixa_link]:https://gitlab.com/lunovox/e-urn/-/archive/master/e-urn-master.zip?inline=false
[bower_icon]:https://img.shields.io/badge/Bower-Projeto-green.svg
[bower_link]:https://gitlab.com/lunovox/e-urn/-/raw/master/textures/bower.json
[GNU AGPL-3.0]:https://gitlab.com/lunovox/e-urn/-/raw/master/LICENSE
[license_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=Download&color=yellow
[license_link]:https://gitlab.com/lunovox/e-urn/-/raw/master/LICENSE
[locale/README.md]:https://gitlab.com/lunovox/e-urn/-/tree/main/locale?ref_type=heads
[minetest_icon]:https://img.shields.io/static/v1?label=Minetest&message=Game&color=brightgreen
[minetest_link]:https://minetest.net
[projeto_icon]:https://img.shields.io/static/v1?label=Projeto&message=GIT&color=red
[projeto_link]:https://gitlab.com/lunovox/e-urn
[wiki_en_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=EN&color=blue
[wiki_en_link]:https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License
[wiki_pt_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=PT&color=blue
[wiki_pt_link]:https://pt.wikipedia.org/wiki/GNU_Affero_General_Public_License



